/*
 * Color changing headband with WS2801 LED strip
 * Copyright (c) 2017 Vencislav Atanasov
 * Connect strip CK to SCK, connect strip SI to MOSI
 * based on
 * SPI testing utility (using spidev driver)
 *
 * Copyright (c) 2007  MontaVista Software, Inc.
 * Copyright (c) 2007  Anton Vorontsov <avorontsov@ru.mvista.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * Compile on Raspberry Pi with gcc spidev_test.c -o spidev_test
 */

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
#define LEDS 11
#define BYTES_PER_LED 3
#define TX_BUF_SIZE LEDS * BYTES_PER_LED

static void pabort(const char *s)
{
    perror(s);
    abort();
}

static const char *device = "/dev/spidev32766.0";
static uint8_t mode;
static uint8_t bits = 8;
static uint32_t speed = 500000;
static uint16_t delay;

double H = 0.0;

// 33 ms (1000 / 33 = 30 FPS)
const struct timespec tim = {0, 33000000L};

struct RGB
{
	unsigned char R;
	unsigned char G;
	unsigned char B;
};

struct HSV
{
	double H;
	double S;
	double V;
};

struct RGB HSVToRGB(struct HSV hsv)
{
	double r = 0, g = 0, b = 0;

	if (hsv.S == 0) {
		r = hsv.V;
		g = hsv.V;
		b = hsv.V;
	}
	else {
		int i;
		double f, p, q, t;
		
		if (hsv.H == 360) {
			hsv.H = 0;
		}
		else {
			hsv.H = hsv.H / 60;
		}

		i = (int)trunc(hsv.H);
		f = hsv.H - i;

		p = hsv.V * (1.0 - hsv.S);
		q = hsv.V * (1.0 - (hsv.S * f));
		t = hsv.V * (1.0 - (hsv.S * (1.0 - f)));

		switch (i) {
			case 0:
				r = hsv.V;
				g = t;
				b = p;
				break;
			case 1:
				r = q;
				g = hsv.V;
				b = p;
				break;
			case 2:
				r = p;
				g = hsv.V;
				b = t;
				break;
			case 3:
				r = p;
				g = q;
				b = hsv.V;
				break;
			case 4:
				r = t;
				g = p;
				b = hsv.V;
				break;
			default:
				r = hsv.V;
				g = p;
				b = q;
				break;
		}
	}

	struct RGB rgb;
	rgb.R = r * 255;
	rgb.G = g * 255;
	rgb.B = b * 255;

	return rgb;
}

static void transfer_single(int fd, uint8_t tx[], size_t len)
{
    int ret;
    struct spi_ioc_transfer tr = {
        .tx_buf = (unsigned long)tx,
        .rx_buf = 0,
        .len = len,
        .delay_usecs = delay,
        .speed_hz = speed,
        .bits_per_word = bits,
    };

    ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
    if (ret < 1) {
        pabort("can't send spi message");
	}
}

static void transfer(int fd)
{
	uint8_t tx[TX_BUF_SIZE];
	size_t i;
	struct HSV led_hsv;

	// H offset for every frame, so the colors rotate
	H += 1;

	for (i = 0; i < LEDS; ++i) {
		H += 360.0 / LEDS;

		while (H > 360) {
				H = H - 360;
		}

		led_hsv.H = H;
		led_hsv.S = 1;
		led_hsv.V = 1;
		struct RGB led_rgb = HSVToRGB(led_hsv);
		tx[i * BYTES_PER_LED] = led_rgb.R;
		tx[i * BYTES_PER_LED + 1] = led_rgb.B;
		tx[i * BYTES_PER_LED + 2] = led_rgb.G;
	}

	transfer_single(fd, tx, ARRAY_SIZE(tx));
}

static void loop_transfer(int fd)
{
	while (1) {
		transfer(fd);
		nanosleep(&tim, NULL);
	}
}

static void print_usage(const char *prog)
{
    printf("Usage: %s [-DsbdlHOLC3]\n", prog);
    puts("  -D --device   device to use (default /dev/spidev0.0)\n"
         "  -s --speed    max speed (Hz)\n"
         "  -d --delay    delay (usec)\n"
         "  -b --bpw      bits per word \n"
         "  -l --loop     loopback\n"
         "  -H --cpha     clock phase\n"
         "  -O --cpol     clock polarity\n"
         "  -L --lsb      least significant bit first\n"
         "  -C --cs-high  chip select active high\n"
         "  -3 --3wire    SI/SO signals shared\n");
    exit(1);
}

static void parse_opts(int argc, char *argv[])
{
    while (1) {
        static const struct option lopts[] = {
            { "device",  1, 0, 'D' },
            { "speed",   1, 0, 's' },
            { "delay",   1, 0, 'd' },
            { "bpw",     1, 0, 'b' },
            { "loop",    0, 0, 'l' },
            { "cpha",    0, 0, 'H' },
            { "cpol",    0, 0, 'O' },
            { "lsb",     0, 0, 'L' },
            { "cs-high", 0, 0, 'C' },
            { "3wire",   0, 0, '3' },
            { "no-cs",   0, 0, 'N' },
            { "ready",   0, 0, 'R' },
            { NULL, 0, 0, 0 },
        };
        int c;

        c = getopt_long(argc, argv, "D:s:d:b:lHOLC3NR", lopts, NULL);

        if (c == -1) {
            break;
		}

        switch (c) {
			case 'D':
				device = optarg;
				break;
			case 's':
				speed = atoi(optarg);
				break;
			case 'd':
				delay = atoi(optarg);
				break;
			case 'b':
				bits = atoi(optarg);
				break;
			case 'l':
				mode |= SPI_LOOP;
				break;
			case 'H':
				mode |= SPI_CPHA;
				break;
			case 'O':
				mode |= SPI_CPOL;
				break;
			case 'L':
				mode |= SPI_LSB_FIRST;
				break;
			case 'C':
				mode |= SPI_CS_HIGH;
				break;
			case '3':
				mode |= SPI_3WIRE;
				break;
			case 'N':
				mode |= SPI_NO_CS;
				break;
			case 'R':
				mode |= SPI_READY;
				break;
			default:
				print_usage(argv[0]);
				break;
        }
    }
}

int main(int argc, char *argv[])
{
    int ret = 0;
    int fd;

    parse_opts(argc, argv);

    fd = open(device, O_RDWR);
    if (fd < 0) {
        pabort("can't open device");
	}

    /*
     * spi mode
     */
    ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
    if (ret == -1) {
        pabort("can't set spi mode");
	}

    ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
    if (ret == -1) {
        pabort("can't get spi mode");
	}

    /*
     * bits per word
     */
    ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
    if (ret == -1) {
        pabort("can't set bits per word");
	}

    ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
    if (ret == -1) {
        pabort("can't get bits per word");
	}

    /*
     * max speed hz
     */
    ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
    if (ret == -1) {
        pabort("can't set max speed hz");
	}

    ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
    if (ret == -1) {
        pabort("can't get max speed hz");
	}

    printf("spi device: %s\n", device);
    printf("spi mode: %d\n", mode);
    printf("bits per word: %d\n", bits);
    printf("max speed: %d Hz (%d KHz)\n", speed, speed/1000);

    loop_transfer(fd);

    close(fd);

    return ret;
}
