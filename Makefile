TARGET=headband_ws2801
CFLAGS=-Wall -Werror
LDFLAGS=-lm

all: $(TARGET)

clean:
	rm -f $(TARGET)
